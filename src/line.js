export const ATTRIBUTE = "data-line-id"

export class Line {
  constructor(lineElement, selection) {
    this.line = !lineElement.tagName
      ? this.replaceTextElement(lineElement, selection)
      : lineElement
    this.createId()
  }

  createId() {
    this.id = "_" + Math.random().toString(36).substr(2, 9)
    this.line.setAttribute(ATTRIBUTE, this.id)
  }

  getId() {
    return this.id
  }

  getLength() {
    return this.line.textContent.length
  }

  hasLength() {
    return !!this.getLength()
  }

  remove() {
    this.line.remove()
  }

  replaceTextElement(lineElement, selection) {
    const range = selection.getRangeAt(selection.rangeCount - 1)
    const {startOffset, endOffset} = range
    const newElement = document.createElement("div")
    newElement.innerHTML = lineElement.textContent
    lineElement.parentElement.replaceChild(newElement, lineElement)

    selection.removeAllRanges()

    const newRange = document.createRange()
    newRange.setStart(newElement.firstChild, startOffset)
    newRange.setEnd(newElement.firstChild, endOffset)

    selection.addRange(newRange)

    return newElement
  }

  heading(tag) {
    const alreadyHeading = this.line.tagName.toLowerCase() === tag.toLowerCase()
    if (alreadyHeading) {
      tag = "div"
    }

    const content = this.line.innerHTML
    const headingElement = document.createElement(tag)
    const parent = this.line.parentElement

    if (!alreadyHeading) {
      this.addStylesToHeading(tag, headingElement)
    }

    headingElement.innerHTML = content
    parent.replaceChild(headingElement, this.line)
    this.line = headingElement
  }

  inline(tag, selection) {
    const range = selection.getRangeAt(selection.rangeCount - 1)
    const node = range.commonAncestorContainer
    const closestTags = this.findClosestInlineTag(node, tag)

    if (closestTags.length) {
      closestTags.map((tag) => {
        const childNodes = tag.childNodes
        childNodes.forEach((node) => {
          tag.parentElement.insertBefore(node.cloneNode(true), tag)
        })
        tag.parentElement.removeChild(tag)
      })

      return
    }

    const b = document.createElement(tag)
    b.appendChild(range.extractContents())
    range.deleteContents()
    range.insertNode(b)
  }

  findClosestInlineTag(node, tag) {
    if (node.nodeName === tag) return [node]
    const parentElement = node.parentElement
    const closestParent =
      parentElement.tagName === tag || parentElement.closest(tag)
    const closestChild = node.childNodes.length
      ? node.childNodes[0].parentElement.getElementsByTagName(tag)
      : []

    return [closestParent, ...closestChild].filter(Boolean)
  }

  addStylesToHeading(tag, element) {
    element.setAttribute("role", "heading")
    element.style.fontWeight = "bold"

    if (tag === "h1") {
      element.setAttribute("aria-level", "1")
      element.style.fontSize = "40px"
    }
    if (tag === "h2") {
      element.setAttribute("aria-level", "2")
      element.style.fontSize = "30px"
    }
  }
}
