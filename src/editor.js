import {ATTRIBUTE, Line} from "./line"

export class Editor {
  constructor(element) {
    this.contentElement = element
    this.lines = []

    this.initControls()
  }

  initControls() {
    const actionButtons = document.getElementsByTagName("button")

    Array.from(actionButtons).forEach((button) => {
      const tagName = button.getAttribute("data-tag")

      if (tagName) {
        button.addEventListener("click", (event) => {
          if (!this.isSelectionInsideEditBlock()) return
          this.getLineElementsFromSelection()
            .map(({element, selection}) => {
              if (!element.tagName || !element.hasAttribute(ATTRIBUTE)) {
                const line = new Line(element, selection)
                this.lines.push(line)

                return {line, selection}
              }
              const lineId = element.getAttribute(ATTRIBUTE)
              const line = this.lines.find((line) => line.getId() === lineId)

              return {line, selection}
            })
            .filter(({line}) => !!line)
            .forEach(({line, selection}) => {
              if (["h1", "h2"].includes(tagName)) {
                line.heading(tagName)
              }
              if (["b", "i"].includes(tagName)) {
                line.inline(tagName, selection)
              }
            })

          this.getSelection().removeAllRanges()
        })
      }
    })
  }

  isSelectionInsideEditBlock() {
    return this.contentElement.contains(window.getSelection().anchorNode)
  }

  getLineElementsFromSelection() {
    const selection = this.getSelection()
    const range = selection.getRangeAt(selection.rangeCount - 1)
    const container = range.commonAncestorContainer

    if (container.hasAttribute && container.hasAttribute(ATTRIBUTE)) {
      return [{element: container, selection}]
    }
    if (container.closest) {
      const closestLine = container.closest(`div[${ATTRIBUTE}]`)
      if (closestLine) {
        return [{element: closestLine, selection}]
      }
    }

    if (selection.anchorNode.isSameNode(selection.extentNode)) {
      if (selection.anchorNode.parentElement.isSameNode(this.contentElement)) {
        return [{element: selection.anchorNode, selection}]
      }
      return [{element: selection.anchorNode.parentElement, selection}]
    }

    return Array.from(container.childNodes)
      .filter((childNode) => selection.containsNode(childNode, true))
      .map((element) => ({element, selection}))
  }

  getSelection() {
    return window.getSelection()
  }
}
